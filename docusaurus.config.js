// @ts-check
// `@type` JSDoc annotations allow editor autocompletion and type checking
// (when paired with `@ts-check`).
// There are various equivalent ways to declare your Docusaurus config.
// See: https://docusaurus.io/docs/api/docusaurus-config

import {themes as prismThemes} from 'prism-react-renderer';

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Cartographie de la Forge des communs numériques éducatifs',
  tagline: 'Commun numérique proposé par la DNE',
  favicon: 'img/forge.png',

  // Set the production url of your site here
  url: 'https://docs.forge.apps.education.fr/',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/cartographie/',

  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'throw',

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: './sidebars.js',
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
         },
        theme: {
          customCss: './src/css/custom.css',
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/logo/Logo_La_Forge_large.png',
      navbar: {
        title: '',
        logo: {
          alt: 'logo forge',
          href: 'https://docs.forge.apps.education.fr/',
          target: '_self',
          src: 'https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/logo/Logo_La_Forge_large.png',
        },
        items: [
          {to: '/', label: '🗺️Carte', position: 'left'},
          {type: 'docSidebar', sidebarId: 'SidebarApplications', position: 'left', label: '📱Applications libres',},
          {type: 'docSidebar', sidebarId: 'SidebarRessources', position: 'left', label: '📚Ressources libres',},
          {type: 'docSidebar', sidebarId: 'SidebarPortails', position: 'left', label: '🌐Portails',},
          {label: 'tags',href: 'https://docs.forge.apps.education.fr/cartographie/docs/tags/',position: 'right',},
        ],

      },
      footer: {
        style: 'dark',
        links: [
          {
           items: [
             {
               label: 'Code source',
               href: 'https://forge.apps.education.fr/docs/cartographie',
             },
            ]
          },
          {
            items: [
              {
                label: 'Proposer un projet',
                href: 'https://forge.apps.education.fr/docs/cartographie/-/issues',
              },
             ]
           }
        ],    
        copyright: `Équipe de rédaction de la documentation de la Forge des Communs Numériques Éducatifs`,
      },
      prism: {
        theme: prismThemes.github,
        darkTheme: prismThemes.dracula,
      },
    }),

  plugins: [
    [
      '@docusaurus/plugin-ideal-image',
      {
        quality: 70,
        max: 1030, // max resized image's size.
        min: 640, // min resized image's size. if original is lower, use that size.
        steps: 2, // the max number of images generated between min and max (inclusive)
        disableInDev: false,
      },
    ],
  ],
};

export default config;
