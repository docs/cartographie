import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Un Moodle pour les enseignants et leurs élèves',
    Svg: require('@site/static/img/moodle-logo.png').default,
    description: (
      <>
        Éléa est un commun numérique proposé par la DNE. C'est une plateforme numérique d'apprentissage adaptée à l'enseignement primaire et secondaire. 
      </>
    ),
  },
  {
    title: 'Accompagner les élèves',
    Svg: require('@site/static/img/evaluation.png').default,
    description: (
      <>
        La plateforme permet aux enseignants de concevoir aisément des parcours pédagogiques scénarisés, de les mettre en œuvre avec leurs élèves et de suivre leur progression
      </>
    ),
  },
  {
    title: 'Des activités variées',
    Svg: require('@site/static/img/mode-de-vie.png').default,
    description: (
      <>
      Différents types d'activités pour favoriser l'autonomie, la différenciation et la collaboration
      </>
    ),
  },

];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <img src={Svg}/>
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
