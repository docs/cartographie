/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* eslint-disable global-require */

import {translate} from '@docusaurus/Translate';
import {sortBy} from '@site/src/utils/jsUtils';

/*
 * ADD YOUR SITE TO THE DOCUSAURUS SHOWCASE
 *
 * Please don't submit a PR yourself: use the Github Discussion instead:
 * https://github.com/facebook/docusaurus/discussions/7826
 *
 * Instructions for maintainers:
 * - Add the site in the json array below
 * - `title` is the project's name (no need for the "Docs" suffix)
 * - A short (≤120 characters) description of the project
 * - Use relevant tags to categorize the site (read the tag descriptions on the
 *   https://docusaurus.io/showcase page and some further clarifications below)
 * - Add a local image preview (decent screenshot of the Docusaurus site)
 * - The image MUST be added to the GitHub repository, and use `require("img")`
 * - The image has to have minimum width 640 and an aspect of no wider than 2:1
 * - If a website is open-source, add a source link. The link should open
 *   to a directory containing the `docusaurus.config.js` file
 * - Resize images: node admin/scripts/resizeImage.js
 * - Run optimizt manually (see resize image script comment)
 * - Open a PR and check for reported CI errors
 *
 * Example PR: https://github.com/facebook/docusaurus/pull/7620
 */

// LIST OF AVAILABLE TAGS
// Available tags to assign to a showcase site
// Please choose all tags that you think might apply.
// We'll remove inappropriate tags, but it's less likely that we add tags.
export type TagType =
  | 'nouveautés'
  | 'école'
  | 'collège'
  | 'lycée'
  | 'application'
  | 'ressource'
  | 'portail'
  | 'coup_cœur';

// Add sites to this list
// prettier-ignore
const Users: User[] = [
  {
    title: 'Image To PixelArt',
    description: 'génère des grilles de pixels à partir d\'une image',
    preview: require('/docs/ApplicationsLibres/img/ImageToPixelArt.png'),
    website: '/docs/ApplicationsLibres/ImageToPixelArt',
    tags: ['école','collège','application','nouveautés'],
    theme:'dessin',
    partenaires:'',
},
{
    title: 'Rosarosae',
    description: 'Ressources pour les lettres classiques',
    preview: require('/docs/RessourcesLibres/img/Rosarosae.png'),
    website: '/docs/RessourcesLibres/Rosarosae',
    tags: ['collège','lycée','ressource','nouveautés'],
    theme:'Latin,Grec',
    partenaires:'',
},
{
    title: 'Modèle AMC',
    description: 'Modèles de fichiers source pour le logiciel Auto Multiple Choice',
    preview: require('/docs/RessourcesLibres/img/ModelesAMC.png'),
    website: '/docs/RessourcesLibres/ModelesAMC',
    tags: ['collège','lycée','ressource','nouveautés'],
    theme:'QCM',
    partenaires:'',
},
{
    title: 'AléaTex',
    description: 'Des exercices à données aléatoires, un export LaTeX',
    preview: require('/docs/ApplicationsLibres/img/AleaTex.png'),
    website: '/docs/ApplicationsLibres/AleaTex',
    tags: ['école','collège','lycée','application','nouveautés'],
    theme:'mathématiques',
    partenaires:'',
},
{
    title: 'FLGames',
    description: 'Quelques jeux éducatifs pour la salle de classe.',
    preview: require('/docs/ApplicationsLibres/img/FLGames.png'),
    website: '/docs/ApplicationsLibres/FLGames',
    tags: ['école','application','nouveautés'],
    theme:'anglais',
    partenaires:'',
},
{
    title: 'Quel collège',
    description: 'Trouver le collège de secteur',
    preview: require('/docs/ApplicationsLibres/img/QuelCollege.png'),
    website: '/docs/ApplicationsLibres/QuelCollege',
    tags: ['école','application','nouveautés'],
    theme:'français',
    partenaires:'',
},
{
    title: 'Dico Défi',
    description: 'S\'entraîner à l\'apprentissage de l\'ordre alphabétique',
    preview: require('/docs/ApplicationsLibres/img/DicoDefi.png'),
    website: '/docs/ApplicationsLibres/DicoDefi',
    tags: ['école','application','nouveautés'],
    theme:'français',
    partenaires:'',
},
{
    title: 'MathAléa',
    description: 'générateur d\'exercices de mathématiques',
    preview: require('/docs/RessourcesLibres/img/MathAlea.png'),
    website: '/docs/RessourcesLibres/MathAlea',
    tags: ['école','collège','lycée','ressource'],
    theme:'mathématiques',
    partenaires:'',
},
{
    title: 'Prérequis Maths Première',
    description: 'Prérequis en maths pour la première.',
    preview: require('/docs/RessourcesLibres/img/prerequis-math-premiere.png'),
    website: '/docs/RessourcesLibres/prerequis-maths-premiere',
    tags: ['lycée','ressource','nouveautés'],
    theme:'mathématiques',
    partenaires:'',
},
{
    title: 'Sismopiezo',
    description: 'Une application permettant d\'enregistrer les vibrations "sismiques" enregistrées à l\'aide de deux pastilles piézométriques',
    preview: require('/docs/ApplicationsLibres/img/Sismopiezo.jpg'),
    website: '/docs/ApplicationsLibres/Sismopiezo',
    tags: ['collège','lycée','application','nouveautés'],
    theme:'SVT',
    partenaires:'',
},
{
    title: 'Sommation temporelle',
    description: 'Application permettant de traiter la sommation temporelle',
    preview: require('/docs/ApplicationsLibres/img/SommationTemporelle.jpg'),
    website: '/docs/ApplicationsLibres/SommationTemporelle',
    tags: ['collège','lycée','application','nouveautés'],
    theme:'SVT',
    partenaires:'',
},
{
    title: 'IRM virtuelle',
    description: 'Application pour simuler grossièrement des IRM fonctionnelles à partir d\'une liste de situations tests et témoins.',
    preview: require('/docs/ApplicationsLibres/img/IRMvirtuelle.jpg'),
    website: '/docs/ApplicationsLibres/IRMvirtuelle',
    tags: ['collège','lycée','application','nouveautés'],
    theme:'SVT',
    partenaires:'',
},
{
    title: 'Genepop',
    description: 'Application permettant de simuler les effets de la dérive génétique, de la sélection naturelle et des mutations',
    preview: require('/docs/ApplicationsLibres/img/Genepop.jpg'),
    website: '/docs/ApplicationsLibres/Genepop',
    tags: ['collège','lycée','application','nouveautés'],
    theme:'SVT',
    partenaires:'',
},
{
    title: 'Origine des saisons',
    description: 'Application qui permet de faire varier différents paramètres astronomiques et d\'en étudier les effets',
    preview: require('/docs/ApplicationsLibres/img/OrigineSaisons.jpg'),
    website: '/docs/ApplicationsLibres/OrigineSaisons',
    tags: ['collège','lycée','application','nouveautés'],
    theme:'SVT',
    partenaires:'',
},
{
    title: 'Édu\'modèles',
    description: 'Application permettant de construire des modèles de 2 types : Analytiques et Algorithmiques ',
    preview: require('/docs/ApplicationsLibres/img/Edumodeles.png'),
    website: '/docs/ApplicationsLibres/Edumodeles',
    tags: ['collège','lycée','application','nouveautés'],
    theme:'SVT',
    partenaires:'',
},
{
    title: 'Equil\'al',
    description: '’Application qui permet de calculer l\'IMC et de confectionner un menu par rapport aux besoins de l\'organisme',
    preview: require('/docs/ApplicationsLibres/img/Equilal.jpg'),
    website: '/docs/ApplicationsLibres/Equilal',
    tags: ['collège','lycée','application','nouveautés'],
    theme:'SVT',
    partenaires:'',
},
{
    title: 'Chronoline',
    description: 'application permettant de créer un chronomètre avec plusieurs phases temporelles',
    preview: require('/docs/ApplicationsLibres/img/Chronoline.png'),
    website: '/docs/ApplicationsLibres/Chronoline',
    tags: ['école','collège','lycée','application','nouveautés'],
    theme:'Chronomètre',
    partenaires:'',
},
{
    title: 'Hélificus',
    description: 'générer les documents utiles à l\'organisation d\'examens blancs',
    preview: require('/docs/ApplicationsLibres/img/Helificus.png'),
    website: '/docs/ApplicationsLibres/Helificus',
    tags: ['collège','lycée','application','nouveautés'],
    theme:'examens',
    partenaires:'',
},
{
    title: 'La fabrique de phrase',
    description: 'permet de créer des étiquettes ou des bandes de mots pour fabriquer des phrases',
    preview: require('/docs/ApplicationsLibres/img/FabriquePhrase.png'),
    website: '/docs/ApplicationsLibres/FabriquePhrase',
    tags: ['école','collège','application','nouveautés'],
    theme:'écriture',
    partenaires:'',
},
  {
    title: 'Culture et outils libres',
    description: 'publications, des articles, des contenus',
    preview: require('/docs/RessourcesLibres/img/CultureOutilsLibres.png'),
    website: '/docs/RessourcesLibres/CultureOutilsLibres',
    tags: ['école','collège','lycée','ressource','nouveautés'],
    theme:'AEFE',
    partenaires:'',
},
  {
    title: 'VicNews',
    description: 'siteweb du journal Collège Victor Vasarely',
    preview: require('/docs/RessourcesLibres/img/VicNews.png'),
    website: '/docs/RessourcesLibres/VicNews',
    tags: ['collège','ressource','nouveautés'],
    theme:'journal',
    partenaires:'',
},
{
    title: 'Écris pour moi',
    description: 'Dictionnaire vocal (Android & iPad), pour obtenir l\'orthographe d\'un mot prononcé',
    preview: require('/docs/ApplicationsLibres/img/EcrisPourMoi.png'),
    website: '/docs/ApplicationsLibres/EcrisPourMoi',
    tags: ['école','application','nouveautés'],
    theme:'école,dictionnaire',
    partenaires:'',
},
  {
    title: 'Nos Créa Cartes',
    description: 'fournir des modèles de cartes (flashcards, cartes personnalisées, etc.), compatibles avec l\'application CréaCarte',
    preview: require('/docs/RessourcesLibres/img/NosCreaCartes.png'),
    website: '/docs/RessourcesLibres/NosCreaCartes',
    tags: ['école','collège','lycée','ressource','nouveautés'],
    theme:'cartes',
    partenaires:'',
},
{
    title: 'Algo-pratique',
    description: 'Ressources en programmation Python et algorithmique pour le lycée avec des exercices à auto-évaluation.',
    preview: require('/docs/RessourcesLibres/img/Algo-pratique.png'),
    website: '/docs/RessourcesLibres/Algo-pratique',
    tags: ['lycée','ressource','nouveautés'],
    theme:'NSI,python,programmation',
    partenaires:'',
},
  {
    title: 'ColorClassMap',
    description: 'Une application de gestion de classe (plan de classse et fonctionnalités pédagogiques)',
    preview: require('/docs/ApplicationsLibres/img/ColorClassMap.png'),
    website: '/docs/ApplicationsLibres/ColorClassMap',
    tags: ['collège','lycée','application','nouveautés'],
    theme:'plan,classe',
    partenaires:'',
},
{
    title: 'Face Privacy',
    description: 'une application web qui permet de flouter facilement les visages',
    preview: require('/docs/ApplicationsLibres/img/faceprivacy_logo.png'),
    website: '/docs/ApplicationsLibres/face_privacy',
    tags: ['école', 'collège','lycée','application'],
    theme:'IA',
    partenaires:'',
},
  {
    title: 'Ada et Zangemann',
    description: 'Lecture en ligne d\'un conte entrelaçant logiciels, skateboard et glace à la framboise',
    preview: require('/docs/RessourcesLibres/img/couverture_ada.jpg'),
    website: '/docs/RessourcesLibres/ada-le-livre',
    tags: ['école', 'ressource'],
    theme:'livre',
    partenaires:'',
},
  {
    title: 'CodEx',
    description: 'Site proposant des exercices d\'apprentissage de l\'algorithmique et de la programmation en Python.',
    preview: require('/docs/RessourcesLibres/img/codex.png'),
    website: '/docs/RessourcesLibres/codex',
    tags: ['lycée', 'ressource'],
    theme:'NSI, SNT',
    partenaires:'',
},
  {
    title: 'e-comBox',
    description: 'e-comBox est une plateforme regroupant des applications métiers dans le domaine du e-commerce.',
    preview: require('/docs/ApplicationsLibres/img/logo_ecombox.png'),
    website: '/docs/ApplicationsLibres/e-combox',
    tags: ['lycée', 'application'],
    theme: 'STMG, commerce',
    partenaires:'',
},
  {
    title: 'Primtux',
    description: 'Un système informatique dédié à l\’apprentissage et conçu par des pédagogues, pour l\’école et la maison.',
    preview: require('/docs/ApplicationsLibres/img/primtux.jpg'),
    website: '/docs/ApplicationsLibres/Primtux',
    tags: ['école', 'application'],
    theme:'Linux',
    partenaires:'',
},
  {
    title: 'La Nuit du Code',
    description: ' un marathon de programmation durant lequel les élèves, par équipes de deux ou trois, ont 6h pour coder un jeu.',
    preview: require('/docs/ApplicationsLibres/img/nuitducode.png'),
    website: '/docs/ApplicationsLibres/nuitducode',
    tags: ['école','collège','lycée','application'],
    theme: 'programmation',
    partenaires:'',
},
  {
    title: 'QCM Cam',
    description: ' Outil en ligne qui permet de collecter des retours élève (évaluation, sondage, vote) ) l\'aide d\'un marqueur imprimé',
    preview: require('/docs/ApplicationsLibres/img/qcmcam-logo.png'),
    website: '/docs/ApplicationsLibres/QCMcam',
    tags: ['école','collège','lycée','application'],
    theme: 'Quiz, qrcode',
    partenaires:'',
},
  {
    title: 'Ubisit',
    description: ' Permet de gérer efficacement vos plans de classe, avec une répartition des élèves totalement ou partiellement aléatoire.',
    preview: require('/docs/ApplicationsLibres/img/ubisit.png'),
    website: '/docs/ApplicationsLibres/ubisit',
    tags: ['école','collège','lycée','application'],
    theme: 'plan, classe,',
    partenaires:'',
},
  {
    title: 'MyMarkmap',
    description: 'Outil pour créer facilement et sans inscription des cartes mentales en ligne',
    preview: require('/docs/ApplicationsLibres/img/markmap.png'),
    website: '/docs/ApplicationsLibres/MyMarkmap',
    tags: ['école','collège','lycée','application'],
    theme: 'Markdown, carte, menatale',
    partenaires:'Région académique Auvergne-Rhône-Alpes',
},
  {
    title: 'Éducajou',
    description: 'Suite d\'ApplicationsLibres pour l\'école primaire ',
    preview: require('/docs/Portails/img/educajou.png'),
    website: '/docs/Portails/educajou',
    tags: ['école','collège','lycée','portail'],
    theme:'',
    partenaires:'',
},
  {
    title: 'Flipbook',
    description: 'Ooutil libre et gratuit pour créer facilement un livre numérique que l\'on peut feuilleter',
    preview: require('/docs/ApplicationsLibres/img/flipbook.png'),
    website: '/docs/ApplicationsLibres/Flipbook',
    tags: ['école','collège','lycée','application'],
    theme: 'Markdown, livre',
    partenaires:'',
},
  {
    title: 'Code Puzzle',
    description: 'Puzzles de Parsons, défis avec tests à valider et devoirs supervisés pour enseigner et apprendre Python ',
    preview: require('/docs/ApplicationsLibres/img/codePuzzle.png'),
    website: '/docs/ApplicationsLibres/codePuzzle',
    tags: ['collège','lycée','application'],
    theme : 'jeu, programmation',
    partenaires:'',
},
  {
    title: 'Cahier Numérique',
    description: 'Un cahier numérique permet d\'avoir, côte à côte, un document de travail  et un environnement de création',
    preview: require('/docs/ApplicationsLibres/img/cahierNumeriques.png'),
    website: '/docs/ApplicationsLibres/cahierNumeriques',
    tags: ['école','collège','lycée','application'],
    theme: 'TP, TD',
    partenaires:'',
},
  {
    title: 'Cours de Philosophie',
    description: 'Cours de philosophie (niveau terminale) et d\'HPL (premières).',
    preview: require('/docs/RessourcesLibres/img/cours-eyssette.png'),
    website: '/docs/RessourcesLibres/cours-eyssette',
    tags: ['lycée','ressource'],
    theme: 'cours',
    partenaires:'',
},
  {
    title: 'Cours de NSI',
    description: 'Cours de NSI (niveau première)',
    preview: require('/docs/RessourcesLibres/img/premiere-nsi.png'),
    website: '/docs/RessourcesLibres/premiere-nsi',
    tags: ['lycée','ressource'],
    theme :'',
    partenaires:'',
},
  {
    title: 'Devoirs-faits',
    description: 'Ressources éducatives destinées aux élèves pour les aider dans leurs devoirs',
    preview: require('/docs/RessourcesLibres/img/devoirs-faits.png'),
    website: '/docs/RessourcesLibres/devoirs-faits',
    tags: ['collège','ressource'],
    theme:'',
    partenaires:'',
},
  {
    title: 'la TeXiothèque',
    description: 'Ressources éducatives en LaTex',
    preview: require('/docs/RessourcesLibres/img/texiotheque.png'),
    website: '/docs/RessourcesLibres/teXiotheque',
    tags: ['collège', 'lycée', 'ressource'],
    theme: '',

    partenaires:'',
},
  {
    title: 'Exercices de génétique humaine',
    description: 'exercices d\'entraînement sur les notions d\'hérédité dans l\'apprentissage en génétique humaine',
    preview: require('/docs/RessourcesLibres/img/heredite.png'),
    website: '/docs/RessourcesLibres/heredite',
    tags: ['collège', 'lycée', 'ressource'],
    theme : 'SVT',
    partenaires:'',
},
  {
    title: 'Enseigner la SNT',
    description: 'Ressources pour l\'enseignement SNT',
    preview: require('/docs/RessourcesLibres/img/snt.png'),
    website: '/docs/RessourcesLibres/snt',
    tags: ['lycée', 'ressource'],
    theme: 'numérique',
    partenaires:'',
},
  {
    title: 'Laboratoire Grothendieck',
    description: 'Laboratoire de Mathématiques  au sein du collège Félicien Joly de Fresnes sur Escaut (59)',
    preview: require('/docs/RessourcesLibres/img/laboratoire-grothendieck.png'),
    website: '/docs/RessourcesLibres/laboratoire-grothendieck',
    tags: ['collège', 'lycée', 'ressource'],
    theme: 'mathématiques',
    partenaires:'',
},
  {
    title: 'JAIME',
    description: 'Journée Éléa de l\'académie de Grenoble ',
    preview: require('/docs/RessourcesLibres/img/jaime.png'),
    website: '/docs/RessourcesLibres/jaime',
    tags: ['collège', 'lycée', 'ressource'],
    theme : 'Moodle',
    partenaires:'Région académique Auvergne-Rhône-Alpes',
},
  {
    title: 'Doctrine technique',
    description: 'doctrine technique du numérique pour l\'éducation, afin de consolider le cadre d\'architecture et de règles communes',
    preview: require('/docs/RessourcesLibres/img/doctrine.png'),
    website: '/docs/RessourcesLibres/doctrine',
    tags: ['ressource'],
    theme : '',
    partenaires:'',
},
  {
    title: 'visite virtuelle',
    description: 'Visite virtuelle du lycée Emmanuel Mounier de Grenoble',
    preview: require('/docs/RessourcesLibres/img/visiteVirtuelle.png'),
    website: '/docs/RessourcesLibres/visiteVirtuelle',
    tags: ['ressource'],
    theme : '',
    partenaires:'',
},
  {
    title: 'SNT Ababsurdo',
    description: 'toute l\'année de SNT, clef en main,',
    preview: require('/docs/RessourcesLibres/img/snt-ababsurdo.png'),
    website: '/docs/RessourcesLibres/snt-ababsurdo',
    tags: ['ressource'],
    theme : '',
    partenaires:'',
},
  {
    title: 'BAX.TEX',
    description: 'Quelques sujets de l\'épreuve de Spécialité Maths en Terminale Générale,',
    preview: require('/docs/RessourcesLibres/img/bactex.png'),
    website: '/docs/RessourcesLibres/bactex',
    tags: ['lycée', 'ressource'],
    theme : '',
    partenaires:'',
},
  {
    title: 'Ressources CPGE',
    description: 'Cours de mathématiques en CPGE',
    preview: require('/docs/RessourcesLibres/img/cpge.png'),
    website: '/docs/RessourcesLibres/cpge',
    tags: ['lycée', 'ressource'],
    theme : '',
    partenaires:'',
},
  {
    title: 'Ressources Numériques',
    description: 'Ressources numériques Collège Uporu (tutoriels Éole)',
    preview: require('/docs/RessourcesLibres/img/uporu.png'),
    website: '/docs/RessourcesLibres/uporu',
    tags: ['collège', 'lycée', 'ressource'],
    theme : '',
    partenaires:'',
},
  {
    title: 'Formation à la forge',
    description: 'document d\'accompagnement de la formation "Forge" proposée par le PCD NSI Normandie',
    preview: require('/docs/RessourcesLibres/img/formation-forge.png'),
    website: '/docs/RessourcesLibres/formation-forge',
    tags: ['lycée', 'ressource'],
    theme : '',
    partenaires:'',
},
  {
    title: 'Manuels Libres',
    description: 'Manuels Libres par et pour les enseignants',
    preview: require('/docs/RessourcesLibres/img/manuels-libres.png'),
    website: '/docs/RessourcesLibres/manuels-libres',
    tags: ['lycée', 'ressource'],
    theme : '',
    partenaires:'',
},
  {
    title: 'Cybersécurité',
    description: 'Informations sur les bonnes pratiques, défis interactifs, présentation d\'outils en matière de sécurité informatique.',
    preview: require('/docs/RessourcesLibres/img/cybersecurite.png'),
    website: '/docs/RessourcesLibres/cybersecurite',
    tags: ['école', 'collège', 'lycée', 'ressource'],
    theme : '',
    partenaires:'',
},
  {
    title: 'Tutos Markdown',
    description: 'Apprendre le Markdown en moins de 5 minutes',
    preview: require('/docs/RessourcesLibres/img/tutomd.png'),
    website: '/docs/RessourcesLibres/tutomd',
    tags: ['lycée', 'ressource'],
    theme : '',
    partenaires:'',
},
  {
    title: 'Créa Cartes',
    description: 'Concevoir et éditer des cartes personnalisées à imprimer.',
    preview: require('/docs/ApplicationsLibres/img/creacarte.png'),
    website: '/docs/ApplicationsLibres/creacarte',
    tags: ['école', 'collège','lycée', 'application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'MonOral.net',
    description: 'Outiller la pratique de l\'oral (primaire et secondaire)',
    preview: require('/docs/ApplicationsLibres/img/monOral.png'),
    website: '/docs/ApplicationsLibres/monOral',
    tags: ['école', 'collège','lycée', 'application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'ChatMD',
    description: 'Un chatbot que vous pouvez configurer par vous-même en Markdown',
    preview: require('/docs/ApplicationsLibres/img/chatmd.png'),
    website: '/docs/ApplicationsLibres/chatmd',
    tags: ['collège','lycée', 'application'],
    theme : '',
    partenaires:'Région académique Auvergne-Rhône-Alpes',
},
  {
    title: 'MarkPage',
    description: 'créer facilement un minisite web ou une application pour smarphone, à partir de Markdown',
    preview: require('/docs/ApplicationsLibres/img/markpage.png'),
    website: '/docs/ApplicationsLibres/markpage',
    tags: ['collège','lycée', 'application'],
    theme : '',
    partenaires:'Région académique Auvergne-Rhône-Alpes',
},
  {
    title: 'Pyxel studio',
    description: 'Développement Python/Pyxel en ligne pour créer et partager des jeux',
    preview: require('/docs/ApplicationsLibres/img/pixel_studio.png'),
    website: '/docs/ApplicationsLibres/Pyxel_studio',
    tags: ['lycée', 'application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'SituLearn',
    description: 'Enrichir les sorties pédagogiques à l\'aide d\'applications mobiles',
    preview: require('/docs/ApplicationsLibres/img/situlearn.png'),
    website: '/docs/ApplicationsLibres/situlearn',
    tags: ['école', 'collège', 'lycée', 'application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'École Inclusive',
    description: 'Enrichir les sorties pédagogiques à l\'aide d\'applications mobiles',
    preview: require('/docs/ApplicationsLibres/img/ecole_inclusive.png'),
    website: '/docs/ApplicationsLibres/Ecole_inclusive',
    tags: ['école', 'collège', 'lycée', 'application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'ePoc',
    description: 'Une solution complète de mobile learning libre et ouverte',
    preview: require('/docs/ApplicationsLibres/img/epoc.png'),
    website: '/docs/ApplicationsLibres/ePoc',
    tags: ['école', 'collège', 'lycée', 'application'],
    theme : '',
    partenaires:'Région académique Auvergne-Rhône-Alpes',
},
  {
    title: 'Planet Alert',
    description: 'Un monde ludique pour une pédagogie stimulante qui articule le monde numérique',
    preview: require('/docs/ApplicationsLibres/img/planet_alert.png'),
    website: '/docs/ApplicationsLibres/planet_alert',
    tags: ['collège', 'lycée', 'application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'Secret Not Tellable',
    description: 'Jeu sérieux sur les contraintes de mot de passe',
    preview: require('/docs/ApplicationsLibres/img/SecretNotTellable.png'),
    website: '/docs/ApplicationsLibres/SecretNotTellable',
    tags: ['lycée', 'application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'MathsMentales',
    description: 'Pour travailler les automatismes et le calcul mental en mathématiques',
    preview: require('/docs/ApplicationsLibres/img/MathsMentales.png'),
    website: '/docs/ApplicationsLibres/MathsMentales',
    tags: ['école', 'collège', 'lycée', 'application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'CahierDeVacances',
    description: 'Entretenir en autonomie leur culture mathématique durant la trêve estivale',
    preview: require('/docs/ApplicationsLibres/img/CahierDeVacances.png'),
    website: '/docs/ApplicationsLibres/CahierDeVacances',
    tags: ['lycée', 'application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'LireCouleur',
    description: 'Pour aider les lecteurs débutants à décoder les mots et les syllabes',
    preview: require('/docs/ApplicationsLibres/img/LireCouleur.png'),
    website: '/docs/ApplicationsLibres/LireCouleur',
    tags: ['école', 'application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'TableauSciences',
    description: 'Tableau virtuel pour la classe en sciences',
    preview: require('/docs/ApplicationsLibres/img/TableauSciences.png'),
    website: '/docs/ApplicationsLibres/TableauSciences',
    tags: ['collège', 'lycée','application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'AutoBD',
    description: 'Créer facilement des bandes dessinées',
    preview: require('/docs/ApplicationsLibres/img/AutoBD.jpg'),
    website: '/docs/ApplicationsLibres/AutoBD',
    tags: ['école','collège', 'lycée','application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'CSE',
    description: 'créer un moteur de recherche personnalisé à partir d\'une liste de sites',
    preview: require('/docs/ApplicationsLibres/img/cse.png'),
    website: '/docs/ApplicationsLibres/cse',
    tags: ['école','collège', 'lycée','application'],
    theme : '',
    partenaires:'Région académique Auvergne-Rhône-Alpes',
},
  {
    title: 'Seyes',
    description: 'Écriture cursive sur une feuille cadriée',
    preview: require('/docs/ApplicationsLibres/img/Seyes.png'),
    website: '/docs/ApplicationsLibres/Seyes',
    tags: ['école','application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'Console',
    description: 'Outil de suivi en temps réel de l\'activité de la forge',
    preview: require('/docs/ApplicationsLibres/img/Console.png'),
    website: '/docs/ApplicationsLibres/Console',
    tags: ['application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'ApiGeom',
    description: 'un logiciel de géométrie dynamique simple et léger',
    preview: require('/docs/ApplicationsLibres/img/ApiGeom.png'),
    website: '/docs/ApplicationsLibres/ApiGeom',
    tags: ['école','collège','lycée','application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'Atelier des problèmes',
    description: 'Résoudre des problèmes : contenu progressif et structuré',
    preview: require('/docs/ApplicationsLibres/img/AtelierProbleme.png'),
    website: '/docs/ApplicationsLibres/AtelierProbleme',
    tags: ['école','application'],
    theme : '',
    partenaires:'Région académique Auvergne-Rhône-Alpes',
},
  {
    title: 'Edupyter',
    description: 'Environnement de développement pour l\'enseignement et l\'apprentissage de Python.',
    preview: require('/docs/ApplicationsLibres/img/Edupyter.png'),
    website: '/docs/ApplicationsLibres/Edupyter',
    tags: ['lycée','application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'Labomep',
    description: 'Exercices d\'apprentissage, d\'entraînement et d\'approfondissement en mathématiques avec suivi',
    preview: require('/docs/ApplicationsLibres/img/Labomep.jpg'),
    website: '/docs/ApplicationsLibres/Labomep',
    tags: ['école','collège','lycée','application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'Instrumenpoche',
    description: 'Créer et lire des animations d\'outils de géométrie :règle, compas, équerre, rapporteur',
    preview: require('/docs/ApplicationsLibres/img/Instrumenpoche.png'),
    website: '/docs/ApplicationsLibres/Instrumenpoche',
    tags: ['école','collège','lycée','application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'MathGraph',
    description: 'Création de figues de géométrie, d\'analyse et de simulation',
    preview: require('/docs/ApplicationsLibres/img/MathGraph.png'),
    website: '/docs/ApplicationsLibres/MathGraph',
    tags: ['école','collège','lycée','application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'CyberEnquete',
    description: 'Jeu en ligne de sensibilisation à la cybersécurité',
    preview: require('/docs/ApplicationsLibres/img/CyberEnquete.png'),
    website: '/docs/ApplicationsLibres/CyberEnquete',
    tags: ['collège','lycée','application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'SACoche',
    description: 'Suivi d\'acquisition de compétences',
    preview: require('/docs/ApplicationsLibres/img/SACoche.png'),
    website: '/docs/ApplicationsLibres/SACoche',
    tags: ['école','collège','lycée','application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'CodePM',
    description: 'Version améliorée de Scratch spécialement conçue pour l\'enseignement. ',
    preview: require('/docs/ApplicationsLibres/img/CodePM.png'),
    website: '/docs/ApplicationsLibres/CodePM',
    tags: ['école','collège','lycée','application'],
    theme : '',
    partenaires:'Région académique Auvergne-Rhône-Alpes',
},
  {
    title: 'QRPrint',
    description: 'Générateur de QR Code avec mise en forme pour impression ',
    preview: require('/docs/ApplicationsLibres/img/QRPrint.png'),
    website: '/docs/ApplicationsLibres/QRPrint',
    tags: ['école','collège','lycée','application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'Combicast',
    description: 'Enregistreur des sons pour les réorganiser et les combiner',
    preview: require('/docs/ApplicationsLibres/img/Combicast.png'),
    website: '/docs/ApplicationsLibres/Combicast',
    tags: ['école','collège','lycée','application'],
    theme : '',
    partenaires:'',
},
  {
    title: 'Primaire',
    description: 'Projets de la Forge utiles à l\'école primaire',
    preview: require('/docs/Portails/img/Primaire.png'),
    website: '/docs/Portails/Primaire',
    tags: ['école','portail'],
    theme : '',
    partenaires:'',
},
  {
    title: 'Markdown',
    description: 'Catalogue pour apprendre et exploiter Markdown',
    preview: require('/docs/Portails/img/Markdown.png'),
    website: '/docs/Portails/Markdown',
    tags: ['école','collège','lycée','portail'],
    theme : '',
    partenaires:'',
},
  {
    title: 'Marklab',
    description: 'Suite d\'applications qui utilisent le Markdown',
    preview: require('/docs/Portails/img/Marklab.png'),
    website: '/docs/Portails/Marklab',
    tags: ['école','collège','lycée','portail'],
    theme : '',
    partenaires:'Région académique Auvergne-Rhône-Alpes',
},
  {
    title: 'DRANE Ac-Grenoble',
    description: 'Projets de la forge auxquels la DRANE Ac-Grenoble participe',
    preview: require('/docs/Portails/img/Drane_grenoble.png'),
    website: '/docs/Portails/Drane_grenoble',
    tags: ['portail'],
    theme : '',
    partenaires:'Région académique Auvergne-Rhône-Alpes',
  },
  /*
  Pro Tip: add your site in alphabetical order.
  Appending your site here (at the end) is more likely to produce Git conflicts.
   */
];

export type User = {
  title: string;
  description: string;
  preview: string | null; // null = use our serverless screenshot service
  website: string;
  tags: TagType[];
  theme: string;
  partenaires: string;
};

export type Tag = {
  label: string;
  description: string;
  color: string;
};

export const Tags: {[type in TagType]: Tag} = {
  nouveautés: {
    label: translate({message: 'nouveauté'}),
    description: translate({
      message: 'Nouveaux dépots',
      id: 'showcase.tag.nouveau.description',
    }),
    color: '#e9669e',
  },

  école: {
    label: translate({message: 'école'}),
    description: translate({
      message: 'Pour les enseignants et élèves du premier degré',
      id: 'showcase.tag.ecole.description',
    }),
    color: '#39ca30',
  },

  collège: {
    label: translate({message: 'collège'}),
    description: translate({
      message: 'Pour les enseignants et élèves de collège',
      id: 'showcase.tag.college.description',
    }),
    color: '#dfd545',
  },

  lycée: {
    label: translate({message: 'lycée'}),
    description: translate({
      message: 'Pour les enseignants et élèves de lycée',
      id: 'showcase.tag.lycee.description',
    }),
    color: '#a44fb7',
  },

  application: {
    label: translate({message: 'application'}),
    description: translate({
      message: 'Une application en ligne ou à installer',
      id: 'showcase.tag.application.description',
    }),
    color: '#127f82',
  },

  ressource: {
    label: translate({message: 'ressource'}),
    description: translate({
      message: 'Des ressources pédagogiques libres',
      id: 'showcase.tag.ressource.description',
    }),
    color: '#fe6829',
  },

  portail: {
    label: translate({message: 'portail'}),
    description: translate({
      message: 'Portail indexant des applications et ressources',
      id: 'showcase.tag.portail.description',
    }),
    color: '#ae9821',
  },

  coup_cœur : {
    label: translate({message: '❤️ coup de cœur'}),
    description: translate({
      message: '❤️ Coup de cœur de Kommit 🦫',
      id: 'showcase.tag.portail.description',
    }),
    color: '#ff0000',
  },
};
export const TagList = Object.keys(Tags) as TagType[];
function sortUsers() {
  let result = Users;
  // Sort by site name
  result = sortBy(result, (user) => user.title.toLowerCase());
  // Sort by favorite tag, favorites first
  result = sortBy(result, (user) => !user.tags.includes('favorite'));
  return result;
}

export const sortedUsers = sortUsers();
